<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<div id="page">
	<div id="header">
		<?php if ($logo) { ?><div id="logo"><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div><?php } ?>
		<?php if ($site_name) { ?><h1><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
		<?php if ($site_slogan) { ?><div class="description"><?php print $site_slogan ?></div><?php } ?>
	</div>
	
	<div id="menulinks">
		<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
	</div>

	<div id="banner">
		<div id="bannerleft">
			<?php if ($bannerleft) { ?> <?php print $bannerleft ?> <?php } ?>
		</div>	

		<div id="bannerright">
			<?php if ($bannerright) { ?> <?php print $bannerright ?> <?php } ?>
		</div>
	</div>
		
	<div id="mainarea">
		<div id="contentarea">
		
	        <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
	        <?php print $breadcrumb ?>
	        <h1><?php print $title ?></h1>
	        <div class="tabs"><?php print $tabs ?></div>
	        <?php if ($show_messages) { print $messages; } ?>
	        <?php print $help ?>
	        <div class="content"><?php print $content; ?></div>
	        <?php print $feed_icons; ?>

		</div>
	</div>
	
	<div id="sidebar">
		<?php if ($right) { ?> <?php print $right ?> <?php } ?>	
	</div>
	
	<div id="footer">
		<div id="footerleft"><?php print $footer_message ?><br/></div>
		<div id="footerright">Designed by <a href="http://www.free-css-templates.com/">Free CSS Templates</a> and Ported by <a href="http://goodwinsolutions.com">Goodwin Web Solutions</a></div>
	</div>

</div>
</body>

</html>